package org.manathome.xlsxplugin.engine;

import java.io.InputStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.manathome.xlsxplugin.engine.reader.ExcelReader;

public class ExcelReaderTest {

  public static final String verySimpleExcelFilePath = "test-files/01.very.simple.excel.file.xlsx";

  @Test
  @DisplayName("read should fail on non existing excel file.")
  void testReadShouldFailOnNonExistingExcelInputFile() {
    Assertions.assertThrows(AssertionError.class, () -> {
      new ExcelReader("this-is-not-an-existing-excel-file.xlsx");
    });
  }

  @Test
  @DisplayName("read should fail on missing excel streame.")
  void testReadShouldFailOnMissingExcelInputStream() {

    InputStream excel = this.getClass()
        .getClassLoader()
        .getResourceAsStream("INVALID-PATH/NOFILE/" + verySimpleExcelFilePath);

    Assertions.assertThrows(NullPointerException.class, () -> {
      new ExcelReader(excel);
    });
  }

  @Test
  @DisplayName("read should pass on existing valid excel stream.")
  void testReadShouldPassOnExistingExcelInputStream() {

    InputStream excel = this.getClass()
        .getClassLoader()
        .getResourceAsStream(verySimpleExcelFilePath);

    new ExcelReader(excel);
  }

}
