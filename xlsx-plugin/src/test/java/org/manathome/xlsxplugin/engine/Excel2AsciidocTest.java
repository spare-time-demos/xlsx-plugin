package org.manathome.xlsxplugin.engine;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.util.Optional;

import org.assertj.core.util.Files;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.manathome.xlsxplugin.engine.reader.ExcelReader;
import org.manathome.xlsxplugin.engine.writer.AsciidocWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Excel2AsciidocTest {

  public static final String markupInitOutputFilePath = "00.doc.header.adoc";
  public static final String markupOutputFilePath = "01.very.simple.excel.file.adoc";

  static final Logger logger = LoggerFactory.getLogger(Excel2AsciidocTest.class);

  @Test
  @DisplayName("writing initial asciidoc header.")
  public void writeAciidocInit() throws Exception {

    try (StringWriter out = new StringWriter()) {
      AsciidocWriter writer = new AsciidocWriter(out);
      writer.init("my headline", Optional.of(LocalDateTime.now()), Optional.ofNullable("me as a author"), Optional.empty());
      logger.debug(out.getBuffer().toString());
      assertThat(out.getBuffer().toString()).as("asciidoc header").contains("me as a author");
      assertThat(out.getBuffer().toString()).as("asciidoc header").startsWith("= my headline");
    }
  }

  @Test
  @DisplayName("asciidoc markup should be created from excel.")
  public void testWriteAsciidocFileFromExcelInputStream() throws IOException {

    InputStream excel = this.getClass().getClassLoader().getResourceAsStream(ExcelReaderTest.verySimpleExcelFilePath);

    try (Writer out = new OutputStreamWriter(new FileOutputStream(markupOutputFilePath), "UTF-8")) {
      AsciidocWriter writer = new AsciidocWriter(out);
      writer.init("my headline from _" + ExcelReaderTest.verySimpleExcelFilePath + "_", 
          Optional.of(LocalDateTime.now()),
          Optional.ofNullable("me as a author"),
          Optional.ofNullable(markupOutputFilePath));

      new ExcelReader(excel).withWriter(new AsciidocWriter(out)).ingest();
    }

    assertThat(Files.contentOf(new File(markupOutputFilePath), "UTF-8"))
        .startsWith("= my headline")
        .contains("anotherstring")
        .contains("����")
        .contains("2018-02-20")
        .contains("-33,01")
        .contains("|===");
  }

}
