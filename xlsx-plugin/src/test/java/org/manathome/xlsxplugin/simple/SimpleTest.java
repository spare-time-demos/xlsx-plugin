package org.manathome.xlsxplugin.simple;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@DisplayName("Simple Test junit5 showcase")
public class SimpleTest {

  @Nested
  @Tag("sample")
  @DisplayName("Feature A")
  class FeatureATest {
    @Test
    void expectStraightThroughProcessing() {
      assertThat("abcdef").as("this string should definitly begin with ab").startsWith("ab");
    }

    @Test
    void expectAnotherStraightThroughProcessing() {

    }
  }

  @Nested
  @Tag("sample")
  @DisplayName("Feature B")
  class FeatureBTest {

    @DisplayName("repeated test")
    @RepeatedTest(value = 3, name = "{displayName} {currentRepetition}/{totalRepetitions}")
    void expectStraightThroughProcessing(RepetitionInfo repetitionInfo) {
      assertThat(repetitionInfo.getCurrentRepetition()).isLessThanOrEqualTo(3);
    }

    @Disabled
    @DisplayName("expect disabled test to be disabled")
    @Test
    void expectFailingTestToBeDisabled() {
      fail("will not work.");
    }
  }
}
