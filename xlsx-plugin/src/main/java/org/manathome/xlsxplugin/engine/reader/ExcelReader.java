package org.manathome.xlsxplugin.engine.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.manathome.xlsxplugin.engine.writer.AsciidocWriter;
import org.manathome.xlsxplugin.util.XlsxAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** excel reading using poi. */
public class ExcelReader {

  static final Logger logger = LoggerFactory.getLogger(ExcelReader.class);

  private AsciidocWriter writer;

  private InputStream excelInputStream;

  /** read excel. */
  public ExcelReader(final String excelFileName) {
    logger.trace("ExcelReader(" + excelFileName + ")");

    File file = new File(excelFileName);
    XlsxAssert.requireTrue(file.exists(), "file " + excelFileName + " not found.");
    XlsxAssert.requireTrue(file.isFile() && file.canRead(), excelFileName + " not a readable file");

    try {
      this.excelInputStream = new FileInputStream(file);
    } catch (FileNotFoundException fnf) {
      XlsxAssert.failedCondition("File not found " + fnf);
    }
  }

  public ExcelReader(final InputStream excelInputStream) {
    XlsxAssert.requireNotNull(excelInputStream, "excel input stream required.");
    this.excelInputStream = excelInputStream;
  }

  /** start parsing excel. */
  public void ingest() {
    XlsxAssert.requireNotNull(excelInputStream, "excel input stream required.");
    XlsxAssert.requireNotNull(writer, "no writer");

    try {
      Workbook workbook = XlsxAssert.requireNotNull(WorkbookFactory.create(excelInputStream), "no workbook");

      logger.trace("Excel Workbook has " + workbook.getNumberOfSheets() + " Sheets. ");
      XlsxAssert.ensureCondition(workbook.getNumberOfSheets() >= 1, "no sheets in excel found");

      Iterator<Sheet> sheetIterator = workbook.sheetIterator();
      while (sheetIterator.hasNext()) {

        Sheet sheet = sheetIterator.next();
        logger.trace("processing sheet: " + sheet.getSheetName());
             
        writer.startTable(Optional.ofNullable(sheet.getSheetName()));

        int rowCount = 0;
        FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
        DataFormatter dataFormatter = new DataFormatter(java.util.Locale.GERMANY);
        Iterator<Row> rowIterator = sheet.rowIterator();

        while (rowIterator.hasNext()) {

          Row row = rowIterator.next();
          rowCount++;

          if (rowCount == 1) {
            writer.startHeader();
          } else {
            writer.startRow();
          }

          // Now let's iterate over the columns of the current row
          Iterator<Cell> cellIterator = row.cellIterator();

          while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();

            String cellValue;
            if (cell.getCellType() == CellType.FORMULA) {
              cellValue = dataFormatter.formatCellValue(cell, formulaEvaluator);
            } else {
              cellValue = dataFormatter.formatCellValue(cell);
            }
            System.out.print(cellValue + "\t");

            if (rowCount == 1) {
              writer.addHeaderColumn(cellValue);
            } else {
              writer.addColumn(cellValue);
            }
          } //cells

          if (rowCount == 1) {
            writer.endHeader();
          } else {
            writer.endRow();
          }
        } //rows

        writer.endTable();
        logger.trace("processed " + rowCount + " excel rows for sheet " + sheet.getSheetName());
        
      } //sheets
      
    } catch (Exception ex) {
      logger.error("ExcelReader could not ingest: " + ex.getMessage(), ex);
      XlsxAssert.failed(ex.getMessage(), ex);
    }
  }

  public AsciidocWriter getWriter() {
    return writer;
  }

  public ExcelReader withWriter(AsciidocWriter writer) {
    this.writer = writer;
    return this;
  }

}
