package org.manathome.xlsxplugin.engine.writer;

import java.io.PrintWriter;
import java.io.Writer;
import java.time.LocalDateTime;
import java.util.Optional;

import org.manathome.xlsxplugin.util.XlsxAssert;

/** markup writer. */
public class AsciidocWriter {

  private final PrintWriter out;

  /** write header. */
  public void init(
      final String header, 
      final Optional<LocalDateTime> date, 
      final Optional<String> author,
      final Optional<String> description) {
    
    out.println("= " + header);
    out.println(date.orElse(LocalDateTime.now()).toString());
    author.ifPresent(a -> out.println(a));
    description.ifPresent(d -> out.println(":description: " + d));
    out.println();
  }

  public AsciidocWriter(final Writer out) {
    this.out = new PrintWriter(XlsxAssert.requireNotNull(out, "no out given"));
  }

  /** begin asciidoc table. */
  public void startTable(final Optional<String> name) {

    out.println();
    out.println();
    if (name.isPresent()) {
      out.println("." + name.get());
    }
    out.println("|===");
  }

  public void startHeader() {
    // nop
  }

  public void endHeader() {
    out.println("");
    out.println("");
  }

  public void addHeaderColumn(String name) {
    out.print("| " + name + " ");
  }

  public void endTable() {
    out.println("|===");
  }

  public void addColumn(String value) {
    out.println("| " + value);
  }

  public void endRow() {
    out.println("");
  }

  public void startRow() {
    // nop
  }

}
