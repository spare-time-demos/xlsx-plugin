package org.manathome.xlsxplugin.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** internal asserts.
 * @author a man@home
 */
public final class XlsxAssert {
  
  static final Logger logger = LoggerFactory.getLogger(XlsxAssert.class);

  private XlsxAssert() {
  }

  /** pre condition. */
  public static void requireTrue(boolean condition, final String conditionFailedMessage) {
    if (!condition) {
      logger.error("failed true: " + conditionFailedMessage);
      throw new AssertionError("required condition not met: " + conditionFailedMessage);
    }
  }

  /** pre condition. */
  public static <T> T requireNotNull(final T notNullObject, final String condition) {
    if (notNullObject == null) {
      logger.error("failed notnull: " + condition);
      throw new NullPointerException("null not allowed on " + condition);
    }
    return notNullObject;
  }

  /** pre condition. */
  public static String requireNotNull(final String nonEmptyString, final String condition) {
    if (nonEmptyString == null) {
      logger.error("failed not null: " + condition);
      throw new NullPointerException("null string not allowed on " + condition);
    }
    if (nonEmptyString.length() == 0) {
      logger.error("failed not 0 length: " + condition);
      throw new NullPointerException("0 length string not allowed on " + condition);
    }

    return nonEmptyString;
  }

  /** post condition. */
  public static void ensureCondition(boolean condition, final String conditionFailedMessage) {
    if (!condition) {
      logger.error("conditation failed: " + conditionFailedMessage);
      throw new AssertionError("condition violation: " + conditionFailedMessage);
    }
  }

  /** post condition. */
  public static void failedCondition(final String conditionFailedMessage) {
    ensureCondition(false, conditionFailedMessage);
  }

  /** post condition. */
  public static void failed(final String conditionFailedMessage, Throwable ex) {
    logger.error("conditation failed: " + conditionFailedMessage, ex);
    throw new AssertionError("condition violation: " + conditionFailedMessage, ex);    
  }
}