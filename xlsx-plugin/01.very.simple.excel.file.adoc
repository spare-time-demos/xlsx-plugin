= my headline from _test-files/01.very.simple.excel.file.xlsx_
2018-10-20T21:08:36.553
me as a author
:description: 01.very.simple.excel.file.adoc



.sample excel sheet 1
|===
| Header1 | Header2 | Header3 | Formula | Money | Date 

| astring
| 1
| a string with blanks
| 3,50
| 12,45 €
| 

| anotherstring
| 2
| 
| 4,50
| 0,00 €
| 2018-10-10

| thirdrow
| 3
| A öäüß \ test
| 5,50
| -33,01 €
| 2018-02-20

|===


.second sheet 2
|===
| Date | Version | Note 

| 01/01/2017
| 1.8.1
| a sample release

| 11/01/2017
| 1.9.0
| another relase

| 01/01/2018
| 2.0.1
| latest and greatest release.
Multi line text

|===
