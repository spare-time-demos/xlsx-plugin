= build

include::dev-build-upandrunning.adoc[leveloffset=+1]

== dev tools

* java jdk
* gradle, build tool            https://gradle.org
* git with gitlab hosting on    https://gitlab.com/spare-time-demos/project-template
* eclipse, ide                  http://www.eclipse.org/
* checkstyle, style checker     http://checkstyle.sourceforge.net/

== libraries

* junit5, unit tests            https://junit.org/junit5/
* assertj, assertion library    http://joel-costigliola.github.io/assertj/
* apache poi

== gradle builds

how to build the project:

.test and run
----
./gradlew test
----

== ci build

see `.gitlab-ci.yml`

== qa/test

include::dev-build-testdata.adoc[leveloffset=+2]

=== automated testing

TBD

=== manual explorative testing

TBD
