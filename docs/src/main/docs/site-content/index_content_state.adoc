= project reports
man@home
2018-10-12
:jbake-type: indexentry
:jbake-tags: management

== Reports

.project reports
[cols="1,3,2"]
|===

| planning  |    https://gitlab.com/spare-time-demos/xlsx-plugin/boards            | gitlab issue board
| building  |    https://gitlab.com/spare-time-demos/xlsx-plugin/pipelines         | gitlab ci build pipeline state
| testing   |    link:reports/tests/test/index.html[]                              | junit5 test results
| changelog |    link:manual/html/project-changelog.generated.html[]               | generated changelog (gitlab issues)
| roadmap   |    link:manual/html/project-roadmap.generated.html[]                 | generated roadmap (gitlab milestones)

|===
