<#include "header.ftl">	
<#include "menu.ftl">
	
	<div id="main">
        <#list published_indexentrys as post>
			<#if (post??) >
				<#include "indexentry.ftl">
			</#if>
		</#list>		
	</div>
	
<#include "commons/sidebar.ftl">
<#include "footer.ftl">