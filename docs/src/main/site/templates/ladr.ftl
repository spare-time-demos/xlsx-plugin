<#include "header.ftl">

	<#include "menu.ftl">
	<div id="main">
		<#assign post = content />
		<#assign titleH1 = true />
		<#if (post??) >
			<#include "ladr/content-single.ftl">
		</#if>
		
		<#include "ladr/prev-next-post.ftl">
    </div>

	<#include "commons/sidebar.ftl">

<#include "footer.ftl">
