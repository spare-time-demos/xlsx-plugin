
<#if (config.sidebar_social_twitter?has_content) >
<li><a href="//twitter.com/${config.sidebar_social_twitter}" target="_blank" title="Twitter" class="fa fa-twitter"></a></li>
</#if>

<#if (config.sidebar_social_email?has_content) >
<li><a href="mailto:${config.sidebar_social_email}" title="Email" class="fa fa-envelope"></a></li>
</#if>
