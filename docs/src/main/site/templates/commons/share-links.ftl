<!-- Social Share Button HTML -->

<!-- Twitter -->
<li><a href="//twitter.com/share?url=${config.site_host}/${post.noExtensionUri!post.uri}&text=${post.title}&via=${config.sidebar_social_twitter}" target="_blank" class="share-btn twitter">
    <i class="fa fa-twitter"></i>
    <p>Twitter</p>
</a></li>

<!-- Email -->
<li><a href="mailto:?subject=Check out this post by ${post.author!config.site_author!config.site_author}&body=${config.site_host}/${post.noExtensionUri!post.uri}" target="_blank" class="share-btn email">
    <i class="fa fa-envelope"></i>
    <p>Email</p>
</a></li>
