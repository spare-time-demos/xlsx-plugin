<#include "header.ftl">

	<#include "menu.ftl">
	<div id="main">
		<#assign post = content />
		<#assign titleH1 = true />
		<#if (post??) >
			<#include "note/content-single.ftl">
		</#if>
		
		<#include "note/prev-next-post.ftl">
    </div>

	<#include "commons/sidebar.ftl">

<#include "footer.ftl">
